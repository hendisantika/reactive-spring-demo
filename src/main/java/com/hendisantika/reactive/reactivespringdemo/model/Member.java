package com.hendisantika.reactive.reactivespringdemo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-spring-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:08
 */
@Data
@Document
public class Member {
    @Id
    private final String id;
    private final String name;
}