package com.hendisantika.reactive.reactivespringdemo.repository;

import com.hendisantika.reactive.reactivespringdemo.model.Member;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-spring-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:09
 */
public interface MemberRepository extends ReactiveCrudRepository<Member, String> {

    @Query("{ 'name': {$regex : '^?0$', $options: 'i'}}")
    Flux<Member> findAllByNameIgnoreCase(String name);

}