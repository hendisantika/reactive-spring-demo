package com.hendisantika.reactive.reactivespringdemo.service;

import com.hendisantika.reactive.reactivespringdemo.model.Member;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-spring-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:10
 */
public interface MemberService {

    Flux<Member> findAllByNameIgnoreCase(String name) throws InterruptedException;
}