package com.hendisantika.reactive.reactivespringdemo.service;

import com.hendisantika.reactive.reactivespringdemo.model.Member;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-spring-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:10
 */
@Service
public class MemberServiceImpl implements MemberService {

    private static final Scheduler SCHEDULER = Schedulers.parallel();

    private final List<Member> members;

    public MemberServiceImpl() {
        this.members = Stream.of(
                new Member("1", "Uzumaki Naruto"),
                new Member("2", "Uchiha Sasuke"),
                new Member("3", "Haruno Sakura"),
                new Member("4", "Hatake Kakashi"),
                new Member("5", "Sarutobi Hiruzen")
        ).collect(Collectors.toList());
    }

    @Override
    public Flux<Member> findAllByNameIgnoreCase(String name) {
        return Flux.fromIterable(members).subscribeOn(SCHEDULER);
    }
}